import React, { useState } from 'react';

export const ScoreContext = React.createContext({
  score: null,
  setScore: () => {},
  resetScore: () => {},
});

const ScoreContextProvider = (props) => {
  const [score, setScore] = useState(0);

  const scoreHandler = (earnedScore) => {
    setScore(score + earnedScore);
  };

  const resetScore = () => {
    setScore(0);
  };

  return (
    <ScoreContext.Provider
      value={{ score: score, setScore: scoreHandler, resetScore: resetScore }}
    >
      {props.children}
    </ScoreContext.Provider>
  );
};
export default ScoreContextProvider;
